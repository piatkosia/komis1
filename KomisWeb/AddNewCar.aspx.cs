﻿using KomisWeb.ServiceReference1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tools;

namespace KomisWeb
{
    public partial class AddNewCar : System.Web.UI.Page
    {
        public NewCar samochod = new NewCar();
        private string base64String;
        private string tmpstring;
        internal Service1SoapClient connector = new Service1SoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void AddThisCar(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                samochod.cena = Convert.ToDecimal(cena.Text);
                samochod.dane_kontaktowe = kontakty.Text + " ";
                samochod.dostepny = true;
                samochod.fotka = imgtemp.Text;
                if (samochod.fotka == null)
                    samochod.fotka = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
                samochod.ilosc_drzwi = Convert.ToInt32(drzwi.Text);
                samochod.kolor_lakieru = lakier.Text+ " ";
                samochod.marka = marka.Text+ " ";
                samochod.model = model.Text+ " ";
                switch (napedlist.SelectedItem.Text)
                {
                    case "Na 4 koła":
                        samochod.naped = 1;
                        break;
                    case "Przedni":
                        samochod.naped = 2;
                        break;
                    case "Tylny":
                        samochod.naped = 3;
                        break;
                    case "Nieznany":
                        samochod.naped = 4;
                        break;
                }
                samochod.numer_rejestracyjny = blachy.Text+ " ";
                samochod.pojemnosc_silnika = Convert.ToInt32(pojemnosc.Text);
                samochod.przebieg = Convert.ToInt32(Przebieg.Text);
                samochod.rocznik = Convert.ToInt32(Przebieg.Text);
                switch (Paliwko.SelectedItem.Text)
                {
                    case "Diesel":
                        samochod.rodzaj_paliwa = 1;
                        break;
                    case "Diesel + gaz":
                        samochod.rodzaj_paliwa = 2;
                        break;
                    case "Benzyna":
                        samochod.rodzaj_paliwa = 3;
                        break;
                    case "Benzyna + gaz":
                        samochod.rodzaj_paliwa = 4;
                        break;

                    case "Prąd":
                        samochod.rodzaj_paliwa = 5;
                        break;
                    case "Inne":
                        samochod.rodzaj_paliwa = 6;
                        break;
                    case "Nieznany":
                        samochod.rodzaj_paliwa = 7;
                        break;

                }
                switch (skrzynka.SelectedItem.Text)
                {
                    case "Ręczna":
                        samochod.skrzynia_biegow = 1;
                        break;
                    case "Automatyczna":
                        samochod.skrzynia_biegow = 2;
                        break;
                    case "Brak danych":
                        samochod.skrzynia_biegow = 3;
                        break;
                }
                samochod.typ = typ.Text+ " ";
                samochod.uwagi = sapy.Text+ " ";
                samochod.fotka = imgtemp.Text;
                if (samochod.fotka.Length < 5) samochod.fotka = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
                string output = NewCar.NewSerializeToXmlString(samochod);
                connector.AddNewCar(output);
                Response.Write("<script>alert('Samochód dodano do bazy.')</script>");
                ClearAll();
                
            }
            else
            {
                Response.Write("<script>alert('Popraw błędy.')</script>");
            }
        }

        protected void fotka_Click(object sender, EventArgs e)
        {
            if (ImgSource.HasFile)
            {
                try
                {
                    Session["image"] = ImgSource.PostedFile;
                    Stream fs = ImgSource.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    imgtemp.Text = base64String;
                    Image1.ImageUrl = "data:image/png;base64," + base64String;

                }
                catch (Exception aException)
                {
                    string wiadomosc = "<script>alert('oops: " + aException.Message + 
                    ".')</script>";
                    Response.Write(wiadomosc);   
                }
            }
        }

      

        public void CleartextBoxes(Control parent)
        {

            foreach (Control x in parent.Controls)
            {
                if ((x.GetType() == typeof(TextBox)))
                {

                    ((TextBox)(x)).Text = "";
                }

                if (x.HasControls())
                {
                    CleartextBoxes(x);
                }
            }
        }

        protected void clear_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void ClearAll()
        {
            CleartextBoxes(this);
            Image1.ImageUrl = null;
            Session["image"] = null;
            imgtemp.Text = null;
        }
    }
}