﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KomisWeb.ServiceReference1;
namespace KomisWeb
{
    public partial class _Default : Page
    {
        private ServiceReference1.Service1SoapClient proxy = new Service1SoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = proxy.GetAvCurrentCars();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

    }
}