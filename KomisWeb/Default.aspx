﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="KomisWeb._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Podgląd klienta - komis samochodowy</h1>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3>Samochody w naszej bazie:</h3>
    <div style="OVERFLOW:auto;WIDTH:900px;HEIGHT:500px" >
    <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999"  BorderWidth="1px" CellPadding="3" BorderStyle="Groove" AutoGenerateColumns="False" >
        <AlternatingRowStyle BackColor="#DCDCDC" />
        <Columns>
            <asp:BoundField DataField="id" HeaderText="#" />
            <asp:BoundField DataField="numer_rejestracyjny" HeaderText=" Nr rejestracyjny " />
            <asp:BoundField DataField="marka" HeaderText="Marka" />
            <asp:BoundField DataField="rocznik" HeaderText=" Rocznik " />
            <asp:BoundField DataField="przebieg" HeaderText=" Przebieg " />
            <asp:BoundField DataField="pojemnosc_silnika" HeaderText=" Poj. silnika [cm^3]" />
            <asp:BoundField DataField="kolor_lakieru" HeaderText=" Kolor lakieru " />
            <asp:BoundField DataField="cena" HeaderText=" Cena " />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"  Width="100"/>
        <PagerStyle ForeColor="Black" HorizontalAlign="Center" BackColor="#999999" />
        <RowStyle BackColor="#EEEEEE" ForeColor="Black"  HorizontalAlign="Center"/>
        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#0000A9" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"></asp:ObjectDataSource>
        </div>
</asp:Content>
