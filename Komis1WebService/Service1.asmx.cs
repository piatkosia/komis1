﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using Tools;

namespace Komis1WebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://localhost.local")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        [WebMethod]
        public bool AddNewCar(string sam)
        {
            NewCar samochod = Tools.NewCar.NewDeserializeFromXml(sam);
            Cars CarDatabase = new Cars();
            string insertstring =
                "INSERT INTO nowe_samochody (numer_rejestracyjny, marka, rocznik, model, typ, przebieg, pojemnosc_silnika, skrzynia_biegow, rodzaj_paliwa, naped, ilosc_drzwi, kolor_lakieru, fotka, cena, dostepny, uwagi, dane_kontaktowe)" +
                          " VALUES (@numer_rejestracyjny, @marka, @rocznik, @model, @typ, @przebieg, @pojemnosc_silnika, @skrzynia_biegow, @rodzaj_paliwa, @naped, @ilosc_drzwi, @kolor_lakieru, @fotka, @cena, @dostepny, @uwagi, @dane_kontaktowe) ";
            Object[] insertparams = new object[17]
            {
               samochod.numer_rejestracyjny+ "", 
               samochod.marka+ " ", 
               samochod.rocznik, 
               samochod.model+ " ",
               samochod.typ+ " ",
               samochod.przebieg,
               samochod.pojemnosc_silnika,
               samochod.skrzynia_biegow,
               samochod.rodzaj_paliwa,
               samochod.naped,
               samochod.ilosc_drzwi,
               samochod.kolor_lakieru+ " ",
               Convert.FromBase64String(samochod.fotka),
               samochod.cena,
               samochod.dostepny,
               samochod.uwagi + " ",
               samochod.dane_kontaktowe
            };

            return CarDatabase.DoInsert(insertstring, insertparams);
        }
       
        [WebMethod]
        public DataTable GetAllCurrentCars()
        {
            Cars CarDatabase = new Cars();
            return CarDatabase.DoSelect("select * from samochody");
        }

        [WebMethod]
        public DataTable GetAllNewCars()
        {
            Cars CarDatabase = new Cars();
            return CarDatabase.DoSelect("select * from nowe_samochody");
        }

        [WebMethod]
        public DataTable GetAvCurrentCars()
        {
            Cars CarDatabase = new Cars();
            return CarDatabase.DoSelect("select * from samochody where dostepny = 1");   
        }
        [WebMethod]
        public DataTable GetCurrentCar(int which)
        {
            Cars CarDatabase = new Cars();
            return CarDatabase.DoSelect("select * from samochody where id = " + which);

        }
        [WebMethod]
        public bool RemoveCar(int which)
        {
            Cars CarDatabase = new Cars();
            return CarDatabase.DoDelete("delete from samochody where id = " + which);
        }

        [WebMethod]
        public DataTable GetNewCar(int which)
        {
            Cars CarDatabase = new Cars();
            return CarDatabase.DoSelect("select * from nowe_samochody where id = " + which);

        }
        [WebMethod]
        public bool RemoveNewCar(int which)
        {
            Cars CarDatabase = new Cars();
            return CarDatabase.DoDelete("delete from nowe_samochody where id = " + which);
        }


        [WebMethod]
        public bool UpdateCurrenyCarInformation(string sam)
        {
            CurrentCar samochod = Tools.CurrentCar.DeserializeFromXml(sam);
            return UpdateCurrentCarInformation2(samochod, samochod.id);
        }
        [WebMethod]
        public bool UpdateCurrentCarInformation2(CurrentCar samochod, int which)
        {
            Cars CarDatabase = new Cars();
            string updatestring = "Update samochody set" +
                " numer_rejestracyjny=@numer_rejestracyjny,"+
                " marka=@marka, "+
                "rocznik=@rocznik,"+
                " model=@model,"+
                " typ=@typ, "+
                "przebieg=@przebieg,"+
                " pojemnosc_silnika=@pojemnosc_silnika,"+
                " skrzynia_biegow=@skrzynia_biegow, "+
                "rodzaj_paliwa=@rodzaj_paliwa, "+
                "naped=@naped, ilosc_drzwi=@ilosc_drzwi,"+
                " kolor_lakieru=@kolor_lakieru, fotka=@fotka, "+
                "cena=@cena, dostepny=@dostepny, uwagi=@uwagi" +
                " Where id = @id";
            Object[] updateparams = new object[17]
            {
               samochod.numer_rejestracyjny+ "", 
               samochod.marka+ " ", 
               samochod.rocznik, 
               samochod.model+ " ",
               samochod.typ+ " ",
               samochod.przebieg,
               samochod.pojemnosc_silnika,
               samochod.skrzynia_biegow,
               samochod.rodzaj_paliwa,
               samochod.naped,
               samochod.ilosc_drzwi,
               samochod.kolor_lakieru+ " ",
               Convert.FromBase64String(samochod.fotka),
               samochod.cena,
               samochod.dostepny,
               samochod.uwagi + " ",
               samochod.id
            };
            return CarDatabase.DoUpdate(updatestring, updateparams);
        }

        [WebMethod]
        public bool AddCurrentCar2(CurrentCar samochod)
        {
            Cars CarDatabase = new Cars();
            string insertstring =
                "INSERT INTO samochody (numer_rejestracyjny, marka, rocznik, model, typ, przebieg, pojemnosc_silnika, skrzynia_biegow, rodzaj_paliwa, naped, ilosc_drzwi, kolor_lakieru, fotka, cena, dostepny, uwagi)" + 
                          " VALUES (@numer_rejestracyjny, @marka, @rocznik, @model, @typ, @przebieg, @pojemnosc_silnika, @skrzynia_biegow, @rodzaj_paliwa, @naped, @ilosc_drzwi, @kolor_lakieru, @fotka, @cena, @dostepny, @uwagi) ";
            Object[] insertparams = new object[16]
            {
               samochod.numer_rejestracyjny+ "", 
               samochod.marka+ " ", 
               samochod.rocznik, 
               samochod.model+ " ",
               samochod.typ+ " ",
               samochod.przebieg,
               samochod.pojemnosc_silnika,
               samochod.skrzynia_biegow,
               samochod.rodzaj_paliwa,
               samochod.naped,
               samochod.ilosc_drzwi,
               samochod.kolor_lakieru+ " ",
               Convert.FromBase64String(samochod.fotka),
               samochod.cena,
               samochod.dostepny,
               samochod.uwagi + " "
            };
           
            return CarDatabase.DoInsert(insertstring, insertparams);
        }

        [WebMethod]
        public bool AddCurrentCar(string sam)
        {
            CurrentCar samochod = Tools.CurrentCar.DeserializeFromXml(sam);
            return AddCurrentCar2(samochod);
            
        }

        [WebMethod]
        public bool MoveCar(int which)
        {
            Cars database = new Cars();
            string commandstring =
                "INSERT INTO samochody (numer_rejestracyjny, marka, rocznik, model, typ, przebieg, pojemnosc_silnika, skrzynia_biegow, rodzaj_paliwa, naped, ilosc_drzwi, kolor_lakieru, fotka, cena, dostepny, uwagi) SELECT numer_rejestracyjny, marka, rocznik, model, typ, przebieg, pojemnosc_silnika, skrzynia_biegow, rodzaj_paliwa, naped, ilosc_drzwi, kolor_lakieru, fotka, cena, dostepny, uwagi FROM nowe_samochody WHERE id = " +
                which.ToString();
           return database.DoMove(commandstring);

        }
    }
}