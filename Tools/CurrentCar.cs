﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Tools
{
    [Serializable]
   // [XmlRoot("CurrentCar")]
    public class CurrentCar
    {

        public int id { get; set; }
        public string numer_rejestracyjny { get; set; }
        public string marka { get; set; }
        public int rocznik { get; set; }
        public string model { get; set; }
        public string typ { get; set; }
        public int przebieg { get; set; }
        public int pojemnosc_silnika { get; set; }
        public int skrzynia_biegow { get; set; }
        public int rodzaj_paliwa { get; set; }
        public int naped { get; set; }
        public int ilosc_drzwi { get; set; }
        public string kolor_lakieru { get; set; }
        //public Byte[] fotka { get; set; }
        public string fotka { get; set; }
        public decimal cena { get; set; }
        public bool dostepny { get; set; }
        public string uwagi { get; set; }

        public static List<CurrentCar> ToList(DataTable tabelka)
        {
            List<CurrentCar> samochody = new List<CurrentCar>();
            if (tabelka.TableName != "bla"){
                throw new SystemException("Dane z dupy. Nie przetwarzam");
            }
            foreach (DataRow rekord in tabelka.Rows)
            {
                CurrentCar samochod = new CurrentCar();
                //używam Convert.Tostring zamiast tostring z obiektu, bo w bazce mogą być nulle.
                samochod.id = Convert.ToInt32(rekord["id"]);
                samochod.numer_rejestracyjny = Convert.ToString(rekord["numer_rejestracyjny"]);
                samochod.marka = Convert.ToString(rekord["marka"]);
                samochod.rocznik = Convert.ToInt32(rekord["rocznik"]);
                samochod.model = Convert.ToString(rekord["model"]);
                samochod.typ = Convert.ToString(rekord["typ"]);
                samochod.przebieg = Convert.ToInt32(rekord["przebieg"]);
                samochod.pojemnosc_silnika = Convert.ToInt32(rekord["pojemnosc_silnika"]);
                samochod.skrzynia_biegow = Convert.ToInt32(rekord["skrzynia_biegow"]);
                samochod.rodzaj_paliwa = Convert.ToInt32(rekord["rodzaj_paliwa"]);
                samochod.ilosc_drzwi = Convert.ToInt32(rekord["ilosc_drzwi"]);
                samochod.kolor_lakieru = Convert.ToString(rekord["kolor_lakieru"]);
                try
                {
                    if (rekord["fotka"] is System.DBNull) samochod.fotka = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
                    else
                    //samochod.fotka = (byte[])rekord["fotka"];
                        samochod.fotka = Convert.ToBase64String((byte[]) rekord["fotka"]);
                }
                catch
                {
                    samochod.fotka = null;
                }
                samochod.cena = Convert.ToDecimal(rekord["cena"]);
                samochod.dostepny = Convert.ToBoolean(rekord["dostepny"]);
                samochod.uwagi = Convert.ToString(rekord["uwagi"]);
                samochod.naped = Convert.ToInt32(rekord["naped"]);
                samochody.Add(samochod);

            }
            return samochody;
        }

        public Bitmap GetImage()
        {
            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
            return (Bitmap)tc.ConvertFrom(fotka);
        }
        public override string ToString()
        {
            return "marka: " + marka + " rocznik: " + rocznik + " numer rejestracyjny: " + numer_rejestracyjny;
        }

        public static string SerializeToXmlString(CurrentCar car)
        {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(CurrentCar));
            //żeby nie sapało że nie może potem base64 odkryptować - daję 1 pikselka 1x1:)
            if (car.fotka == null) car.fotka = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
            serializer.Serialize(writer, car);
            return writer.ToString();
        }

        public static CurrentCar DeserializeFromXml(string p)
        {
            StringReader reader = new StringReader(p);
            XmlSerializer serializer = new XmlSerializer(typeof(CurrentCar));
            CurrentCar car = (CurrentCar)serializer.Deserialize(reader);
            return car;
        }
    }
}
