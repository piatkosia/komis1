﻿using System.Drawing;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml.Serialization;

namespace Tools
{
    [Serializable]
    public class NewCar : CurrentCar
    {
        //skoro się różnią tylko jednym polem, to czemu nie tak:p
        public string dane_kontaktowe { get; set; }
        public static  List<NewCar> ToList2(DataTable tabelka)
        {
            List<NewCar> samochody = new List<NewCar>();
            if (tabelka.TableName != "bla")
            {
                throw new SystemException("Dane z dupy. Nie przetwarzam");
            }
            foreach (DataRow rekord in tabelka.Rows)
            {
                NewCar samochod = new NewCar();
                //używam Convert.Tostring zamiast tostring z obiektu, bo w bazce mogą być nulle.
                samochod.id = Convert.ToInt32(rekord["id"]);
                samochod.numer_rejestracyjny = Convert.ToString(rekord["numer_rejestracyjny"]);
                samochod.marka = Convert.ToString(rekord["marka"]);
                samochod.rocznik = Convert.ToInt32(rekord["rocznik"]);
                samochod.model = Convert.ToString(rekord["model"]);
                samochod.typ = Convert.ToString(rekord["typ"]);
                samochod.przebieg = Convert.ToInt32(rekord["przebieg"]);
                samochod.pojemnosc_silnika = Convert.ToInt32(rekord["pojemnosc_silnika"]);
                samochod.skrzynia_biegow = Convert.ToInt32(rekord["skrzynia_biegow"]);
                samochod.rodzaj_paliwa = Convert.ToInt32(rekord["rodzaj_paliwa"]);
                samochod.kolor_lakieru = Convert.ToString(rekord["kolor_lakieru"]);
                try
                {
                    //samochod.fotka = (byte[])rekord["fotka"];
                    samochod.fotka = Convert.ToBase64String((byte[])rekord["fotka"]);
                }
                catch
                {
                    //tutaj niech wstawia jakąś defaultową
                }
                samochod.cena = Convert.ToDecimal(rekord["cena"]);
                samochod.dostepny = Convert.ToBoolean(rekord["dostepny"]);
                samochod.dane_kontaktowe = Convert.ToString(rekord["dane_kontaktowe"]);
                samochod.uwagi = Convert.ToString(rekord["uwagi"]);
                samochody.Add(samochod);

            }
            return samochody;
        }
        public static string NewSerializeToXmlString(NewCar car)
        {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(NewCar));
            //żeby nie sapało że nie może potem base64 odkryptować - daję 1 pikselka 1x1:)
            if (car.fotka == null) car.fotka = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
            serializer.Serialize(writer, car);
            return writer.ToString();
        }

        public static NewCar NewDeserializeFromXml(string p)
        {
            StringReader reader = new StringReader(p);
            XmlSerializer serializer = new XmlSerializer(typeof(NewCar));
            NewCar car = (NewCar)serializer.Deserialize(reader);
            return car;
        }
    }

}