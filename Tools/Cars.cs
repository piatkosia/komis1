﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Data;

namespace Tools
{

    public class Cars
    {
        CurrentCar car;
        SqlDataAdapter sqlDA = null;
        SqlConnection sqlConnection = new SqlConnection(@"Data Source=A-KUKU-2;Initial Catalog=Cars;Integrated Security=True; Initial Catalog=Cars;Integrated Security=SSPI;");
        public Cars()
        {
            sqlDA = new SqlDataAdapter();
        }

        public DataTable DoSelect(string SqlCommandText)
        {
            SqlCommand selectCommand = new SqlCommand(SqlCommandText);
            selectCommand.Connection = sqlConnection;
            sqlDA.SelectCommand = selectCommand;
            DataTable retdt = new DataTable();
            sqlDA.Fill(retdt);
            retdt.TableName = "bla";
            return retdt;
        }

        //Użycie: DoInsert("Insert into Tabelka (dana1, dana2) values (@d1, @d2)", zmienna1, "jakis_tekst");
        public bool DoInsert(string SqlCommandText, params object[] args)
        {
            SqlCommand insertCommand = new SqlCommand(SqlCommandText);
            string argInCmd = null;
            Regex r = new Regex(@"@(\S+)", RegexOptions.IgnoreCase);
            int i = 0;
            if (r.Matches(SqlCommandText).Count == args.Length)
            {
                foreach (object arg in args)
                {
                    argInCmd = r.Matches(SqlCommandText)[i].ToString();
                    argInCmd = argInCmd.Trim().TrimEnd(new char[] { ')', ',' });
                    insertCommand.Parameters.AddWithValue(argInCmd, arg);
                    i++;
                }
            }
            else
                return false;
            insertCommand.Connection = sqlConnection;
            sqlConnection.Open();
            bool ret = insertCommand.ExecuteNonQuery() > 0;
            sqlConnection.Close();
            return ret;
        }

        public bool DoUpdate(string SqlCommandText, params object[] args)
        {
            SqlCommand updateCommand = new SqlCommand(SqlCommandText);
            string argInCmd = null;
            Regex r = new Regex(@"@(\S+)", RegexOptions.IgnoreCase);
            int i = 0;
            if (r.Matches(SqlCommandText).Count == args.Length)
            {
                foreach (object arg in args)
                {
                    argInCmd = r.Matches(SqlCommandText)[i].ToString();
                    argInCmd = argInCmd.Trim().TrimEnd(new char[] { ')', ',' });
                    updateCommand.Parameters.AddWithValue(argInCmd, arg);
                    i++;
                }
            }
            else
                return false;
            updateCommand.Connection = sqlConnection;
            sqlConnection.Open();
            bool ret = updateCommand.ExecuteNonQuery() > 0;
            sqlConnection.Close();
            return ret;
        }

        public bool DoDelete(string SqlCommandText)
        {
            SqlCommand deleteCommand = new SqlCommand(SqlCommandText);
            deleteCommand.Connection = sqlConnection;
            sqlConnection.Open();
            bool ret = deleteCommand.ExecuteNonQuery() > 0;
            sqlConnection.Close();
            return ret;
        }

        public bool DoMove(string SqlCommandText)
        {
            SqlCommand moveCommand= new SqlCommand(SqlCommandText);
            moveCommand.Connection = sqlConnection;
            sqlConnection.Open();
            bool ret = moveCommand.ExecuteNonQuery() > 0;
            sqlConnection.Close();
            return ret;
        }
    }
}