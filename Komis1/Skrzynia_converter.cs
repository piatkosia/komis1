﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Komis1
{
    class Skrzynia_converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int)value == 1) return "ręczna";
            if ((int)value == 2) return "automatyczna";
            return "brak danych";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
