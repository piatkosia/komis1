﻿using System.Collections.Generic;
using Komis1.ServiceReference1;
using System.Windows;
using System.Windows.Controls;
using Tools;

namespace Komis1
{
    /// <summary>
    /// Interaction logic for NewCarBrowser.xaml
    /// </summary>
    public partial class NewCarBrowser : Window
    {
        internal Service1SoapClient connector;
        private List<NewCar> listasamochodow; 
        public NewCarBrowser()
        {
            InitializeComponent();
            connector = new Service1SoapClient();
            RefreshList();
        }

        private void RefreshList()
        {
            listasamochodow = NewCar.ToList2(connector.GetAllNewCars());
            tmp.DataContext = listasamochodow;
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshList();
        }

        private void ShowCar(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button)sender;
            if (cmd.DataContext is NewCar)
            {
                NewCar samochod = (NewCar)cmd.DataContext;
                InfoForNew info = new InfoForNew { which = samochod.id };
                info.Show();
            }
        }

        private void DelCar(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button)sender;
            if (cmd.DataContext is NewCar)
            {
                NewCar samochod = (NewCar)cmd.DataContext;
                connector.RemoveNewCar(samochod.id);
                MessageBox.Show("Samochód usunięto");
                RefreshList();
            }
        }

        private void AddCar(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button)sender;
            if (cmd.DataContext is NewCar)
            {
                NewCar samochod = (NewCar)cmd.DataContext;
                connector.MoveCar(samochod.id);
                connector.RemoveNewCar(samochod.id);
                MessageBox.Show("Propozycja zaakceptowana");
                RefreshList();
            }
        }
    }
}
