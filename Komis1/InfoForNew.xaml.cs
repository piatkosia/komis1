﻿using Komis1.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tools;

namespace Komis1
{
    /// <summary>
    /// Interaction logic for InfoForNew.xaml
    /// </summary>
    public partial class InfoForNew : Window
    {
        internal Service1SoapClient connector;
        public int which;
        private NewCar samochod;
        public InfoForNew()
        {
            InitializeComponent();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            connector = new Service1SoapClient();
            samochod = (NewCar.ToList2(connector.GetNewCar(which)))[0];
            grid1.DataContext = samochod;
        }
    }
}
