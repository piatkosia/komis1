﻿//using Komis1.ServiceReference1;

using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Komis1.ServiceReference1;
using CurrentCar = Tools.CurrentCar;

namespace Komis1
{
    /// <summary>
    /// Interaction logic for CarBrowser.xaml
    /// </summary>
    public partial class CarBrowser : Window
    {
        internal Service1SoapClient connector;
        List<CurrentCar> listasamochodow;
        public CarBrowser()
        {
            InitializeComponent();
            connector = new Service1SoapClient();
           // DataTable tabelka = connector.GetAllCurrentCars();
            listasamochodow = CurrentCar.ToList(connector.GetAllCurrentCars());
            tmp.DataContext = listasamochodow;
            //tmp1.ItemsSource = tabelka.DefaultView;
        }

        private void AddCar_Click(object sender, RoutedEventArgs e)
        {
            AddNewCar nowy = new AddNewCar();
            nowy.Show();
        }

        private void ShowCar(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button)sender;
            if (cmd.DataContext is CurrentCar)
            {
                CurrentCar samochod = (CurrentCar)cmd.DataContext;
                MoreInfoPage more = new MoreInfoPage {which = samochod.id};
                more.Show();
            }
        }

        private void DelCar(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button)sender;
            if (cmd.DataContext is CurrentCar)
            {
                CurrentCar samochod = (CurrentCar)cmd.DataContext;
                connector.RemoveCar(samochod.id);
                MessageBox.Show("Samochód usunięto");
                RefreshCarList();
            }
        }

        private void RefreshCarList()
        {
            listasamochodow = CurrentCar.ToList(connector.GetAllCurrentCars());
            tmp.DataContext = listasamochodow;
        }

        private void ChangeCar(object sender, RoutedEventArgs e)
        {
            Button cmd = (Button)sender;
            CurrentCar samochod = (CurrentCar)cmd.DataContext;
            EditCarBrowser win = new EditCarBrowser();
            win.samochod = samochod;
            win.Show();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshCarList();
        }
    }
}
