﻿using System;
using System.Windows;
using Komis1.ServiceReference1;
using CurrentCar = Tools.CurrentCar;

namespace Komis1
{
    /// <summary>
    /// Interaction logic for MoreInfoPage.xaml
    /// </summary>
    public partial class MoreInfoPage : Window
    {
        internal Service1SoapClient connector;
        public int which;
        CurrentCar samochod;
        public MoreInfoPage()
        {
            InitializeComponent();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            connector = new Service1SoapClient();
            samochod = (CurrentCar.ToList(connector.GetCurrentCar(which)))[0];
            grid1.DataContext = samochod;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           
        }
    }
}
