﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Komis1
{
    class boolToBrushConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value == true) return new SolidColorBrush(Colors.Blue);
            if ((bool)value == false) return new SolidColorBrush(Colors.Red);
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
