﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Komis1
{
    class FuelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int)value == 1) return "diesel";
            if ((int)value == 2) return "diesel+gaz";
            if ((int)value == 3) return "benzyna";
            if ((int)value == 4) return "benzyna+gaz";
            if ((int)value == 5) return "prąd";
            if ((int)value == 6) return "inne";
            if ((int)value == 7) return "nieznany";
            return "brak danych";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
