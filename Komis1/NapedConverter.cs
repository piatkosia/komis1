﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Komis1
{
    class NapedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int)value == 1) return "przedni";
            if ((int)value == 2) return "tylny";
            if ((int)value == 3) return "dwuosiowy";
            if ((int)value == 4) return "nieznany";
            return "brak danych";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
