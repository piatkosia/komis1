﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Komis1.ServiceReference1;
using Microsoft.Win32;
using CurrentCar = Tools.CurrentCar;

namespace Komis1
{
    /// <summary>
    /// Interaction logic for AddNewCar.xaml
    /// </summary>
    //
    public partial class AddNewCar : Window
    {
        CurrentCar samochod = new CurrentCar();
        internal Service1SoapClient connector = new Service1SoapClient();
        public AddNewCar()
        {
            InitializeComponent();
            Grid1.DataContext = samochod;
            samochod.dostepny = true;
        }

        private void paliwko_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (GetValueFromCombo(Paliwko))
            {
                case "Diesel":
                    samochod.rodzaj_paliwa = 1;
                    break;
                case "Diesel + gaz":
                    samochod.rodzaj_paliwa = 2;
                    break;
                case "Benzyna":
                    samochod.rodzaj_paliwa = 3;
                    break;
                case "Benzyna + gaz":
                    samochod.rodzaj_paliwa = 4;
                    break;

                case "Prąd":
                    samochod.rodzaj_paliwa = 5;
                    break;
                case "Inne":
                    samochod.rodzaj_paliwa = 6;
                    break;
                case "Nieznany":
                    samochod.rodzaj_paliwa = 7;
                    break;


            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            samochod = new CurrentCar();
            Grid1.DataContext = samochod;
            Obrazek = null;
            Osie.SelectedIndex = 0;
            Skrzynia.SelectedIndex = 0;
            Paliwko.SelectedIndex = 0;

        }

        private void ChangePicture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog newfile = new OpenFileDialog
                {
                    Filter = "Zdjęcia(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp"
                };
                if (newfile.ShowDialog() != true) return;
                Obrazek.Source = new BitmapImage(new Uri(newfile.FileName));
                samochod.fotka = Convert.ToBase64String(GetPhoto(newfile.FileName));
            }
            catch
            {
                Debug.Print("Nie udało się obrazka zmienić");
            }
        }
        public static byte[] GetPhoto(string filePath)
        {
            FileStream stream = new FileStream(
                filePath, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(stream);
            byte[] photo = reader.ReadBytes((int)stream.Length);
            reader.Close();
            stream.Close();
            return photo;
        }

        private void NumbersDotOnly(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void osie_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            switch (GetValueFromCombo(Osie))
            {
                case "Na 4 koła":
                    samochod.naped = 1;
                    break;
                case "Przedni":
                    samochod.naped = 2;
                    break;
                case "Tylny":
                    samochod.naped = 3;
                    break;
                case "Nieznany":
                    samochod.naped = 4;
                    break;

            }
        }

        private string GetValueFromCombo(ComboBox box)
        {
            ComboBoxItem item = (ComboBoxItem)box.SelectedItem;
            return item.Content.ToString();
        }

        private void AlfanumericOnly(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^a-zA-Z0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void NumericOnly(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Skrzynia_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (GetValueFromCombo(Skrzynia))
            {
                case "Ręczna":
                    samochod.skrzynia_biegow = 1;
                    break;
                case "Automatyczna":
                    samochod.skrzynia_biegow = 2;
                    break;
                case "Brak danych":
                    samochod.skrzynia_biegow = 3;
                    break;
            }
        }

        private void AddCar_Click(object sender, RoutedEventArgs e)
        {
           //bool wynik = connector.AddCurrentCar(samochod);]
            bool wynik = connector.AddCurrentCar(CurrentCar.SerializeToXmlString(samochod));
            this.Close();
            MessageBox.Show(wynik == true ? "Samochód dodano" : "Błąd dodania.");
        }
    }
}
