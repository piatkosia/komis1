﻿using System.Windows;
using Komis1.ServiceReference1;

namespace Komis1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal Service1SoapClient connector;
        public MainWindow()
        {
            connector = new Service1SoapClient();
            InitializeComponent();
            
        }


        private void General(object sender, RoutedEventArgs e)
        {
            CarBrowser win = new CarBrowser();
            win.Show();
            this.WindowState = WindowState.Minimized;
        }

        private void Waiting(object sender, RoutedEventArgs e)
        {
           NewCarBrowser win = new NewCarBrowser();
            win.Show();
            this.WindowState = WindowState.Minimized;
        }
    }
}
